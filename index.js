const { request } = require("express");
const express = require("express");
const mongoose = require("mongoose"); //manipulate db
const app = express();
const port = 3001;

//Creating connection to MongoDB
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.tovusyc.mongodb.net/b203_to_do?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

//connection check
let db = mongoose.connection;
//if con error
db.on("error", console.error.bind(console, "connection error"));
//if con okay
db.once("open", () => console.log("Cloud DB Connection - OK"));

//Mongoose Schema

const taskSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, "Task is required!"]
    },
    status: {
        type: String,
        default: "pending"
    }
});

//Models
const Task = mongoose.model("Task", taskSchema);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//create
app.post("/tasks", (req, res) => {
    Task.findOne({
            name: req.body.name
        },
        (err, result) => {
            if (result != null && result.name == req.body.name) {
                return res.send("Duplicate task found!");
            } else {
                let newTask = new Task({
                    name: req.body.name
                });
                newTask.save((saveErr, savedTasks) => {
                    if (saveErr) {
                        return console.error(saveErr);
                    } else {
                        return res.status(201).send("New Task Created!");
                    }
                })
            }
        });
});

app.get("/tasks", (req, res) => {
    Task.find({}, (err, result) => {
        if (err) {
            return console.log(err);
        } else {
            return res.status(200).send({
                data: result
            })
        }
    })
});



//Mini Activity
const usersSchema = mongoose.Schema({
    username: {
        type: String,
        required: [true, "Username is required!"]
    },
    password: {
        type: String,
        required: [true, "Password is required!"]
    }
});

const User = mongoose.model("users", usersSchema);

//Activity
app.post("/signup", (req, res) => {
    User.findOne({
            username: req.body.username
        },
        (err, result) => {
            if (result != null && result.username == req.body.username) {
                return res.send("User is already existing!");
            } else {
                let newUser = new User({
                    username: req.body.username,
                    password: req.body.password
                });
                newUser.save((saveErr, savedUser) => {
                    if (saveErr) {
                        return console.error(saveErr);
                    } else {
                        return res.status(201).send("Registered Successfully!");
                    }
                })
            }
        });
});

//view users
app.get("/users", (req, res) => {
    User.find({}, (err, result) => {
        if (err) {
            return console.log(err);
        } else {
            return res.status(200).send({
                data: result
            })
        }
    })
});




app.listen(port, () => console.log(`SERVER IS RUNNING AT PORT: ${port}`));